# Equery
"Equery" è un videogioco di sfida dialogica in modalità arcade. Il gioco è inerente al progetto "Play4STEM", un'iniziativa finanziata dal Fondo di Ateneo per la Ricerca di UniMoRe e che si colloca nel contesto di ricerca della Game Science.

Una demo del videogioco è stata presentata al Festival del Gioco "Play" di Modena. Ulteriori informazioni su di esso sono disponibili nella relativa sezione del programma del festival: [Equery: Sfida Le Domande!](https://www.play-modena.it/2022/programma/equery-sfida-le-domande-73/).

L’applicazione è stata creata tramite l’ambiente di sviluppo per videogiochi Godot e il suo linguaggio GDScript.

L'attuale demo di Equery è giocabile visitando il seguente link: [equery.play4stem.org/](https://equery.play4stem.org/). 