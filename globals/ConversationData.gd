extends Node


var conversations_folder : String = "res://conversations/data/"
var conversations_file : String = conversations_folder + "conversations.json"
var conversations_info := []
var conversation_content := {}
var current_conversation : String = "" 
var current_conversation_id := -1
var available_conversation_ids := []


func _ready() -> void:
	Global.connect("time_of_day_changed", self, "_on_time_of_day_changed")
	conversations_info = load_file(conversations_file)
	available_conversation_ids = Global.get_current_conversation_ids()


func load_file(file_path):
	var file = File.new()
	assert(file.file_exists(file_path))

	file.open(file_path, file.READ)
	var content = parse_json(file.get_as_text())
	assert(content.size() > 0)
	return content


func get_conversation_file_path(conversation_id) -> String:
	return conversations_folder + "conversation_" + str(conversation_id) + ".json"


func get_conversation_content(id : int) -> Dictionary:
	assert(conversations_info.size() > 0)
	conversation_content["title"] = conversations_info[id - 1].title
	conversation_content["desc"] = conversations_info[id - 1].desc
	conversation_content["reputation_needed"] = conversations_info[id - 1].reputation_needed
	conversation_content["reputation_reward"] = conversations_info[id - 1].reputation_reward
	conversation_content["exp_needed"] = conversations_info[id - 1].exp_needed
	conversation_content["max_exp_reward"] = conversations_info[id - 1].max_exp_reward
	return conversation_content


func _on_time_of_day_changed():
	available_conversation_ids = Global.get_current_conversation_ids()
