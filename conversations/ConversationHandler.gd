extends Node

class_name ConversationHandler

signal content_updated

var question: Dictionary = {}
var answers: Array = []

var _conversation: Dictionary
var _current_question_index := 0
var _current_answer_index := 0
var _use_alt_text := false


func load_conversation(file_path) -> Dictionary:
	var file = File.new()
	assert(file.file_exists(file_path))

	file.open(file_path, file.READ)
	var conversation = parse_json(file.get_as_text())
	assert(conversation.size() > 0)
	return conversation


func start_conversation():
	_conversation = load_conversation(ConversationData.current_conversation)
	_current_question_index = 0
	_update()


func get_question_index(id):
	for index in range(0, _conversation["questions"].size()):
		if _conversation["questions"][index].id == id:
			return index
		index += 1


func check_next_question_id(answer_id):
	var next_question_id = answers[answer_id].next_question_id
	if next_question_id.ends_with("b"):
		_use_alt_text = true
		next_question_id = next_question_id.rstrip("b")
	return next_question_id


func next_question(id):
	_current_question_index = get_question_index(id)
	assert(_current_question_index <= _conversation["questions"].size())
	_update()


func _update():
	question = _conversation["questions"][_current_question_index]
	if _use_alt_text:
		question.text = question.alt_text
		_use_alt_text = false
	answers.clear()
	var answer
	for answer_index in range(0, 4):
		answer = _conversation["questions"][_current_question_index]["answers"][answer_index]
		answers.append(answer)
	randomize()
	answers.shuffle()
	emit_signal("content_updated")
