extends Control


onready var _scene_transition = $SceneTransitionOverlay

func _ready() -> void:
	$LineEdit.grab_focus()


func _on_LineEdit_text_entered(new_text: String) -> void:
	if new_text.empty() or not new_text.is_valid_identifier():
		_show_warning_message("Inserisci un nickname valido.")
		return
	if _is_nickname_taken(new_text):
		_show_warning_message("Il nickname è già stato preso.")
	else:
		PlayerData.nickname = new_text
		_scene_transition.transition_to(_scene_transition.next_scene_path)


func _show_warning_message(text):
	$Message.text = text
	$Message.visible = true


func _is_nickname_taken(name) -> bool:
	for item in SilentWolf.Scores.scores:
		if item.player_name == name:
			return true
	return false
