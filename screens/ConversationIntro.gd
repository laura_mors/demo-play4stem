extends Control



onready var _conversation_preview = $PickConversationButton
onready var _user_interface = $UserInterface
onready var _scene_transition = $SceneTransitionOverlay


func _ready() -> void:
	$Player.texture = load(PlayerData.job.file_path)
	_conversation_preview.update_content(ConversationData.current_conversation_id)

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_accept"):
		set_process_input(false)
		
		_scene_transition.play_fade()
		yield(get_tree().create_timer(1.0), "timeout")
		
		_set_up_interface()
		_scene_transition.play_fade_backwards()
		yield(get_tree().create_timer(1.0), "timeout")
		_scene_transition.transition_to(_scene_transition.next_scene_path)


func _set_up_interface():
	_conversation_preview.visible = false
	$Player.visible = true
	$Opponent.visible = true
	_user_interface.change_time_of_day()
	_user_interface.visible = true
